package com.mycompany.WashingApp.logic;

import com.mycompany.WashingApp.entity.Order;
import com.mycompany.WashingApp.entity.OrderItem;

import java.util.List;
import java.util.Random;
import java.util.Set;

public class ExternalOrderOperation {
    private static final int ORDERS_LENGTH = 13;

    public static String getRandomOrderName() {
        String s = "123456789";
        StringBuffer phoneNumber = new StringBuffer();

        for (int i = 0; i < ORDERS_LENGTH; i++) {
            phoneNumber.append(s.charAt(new Random().nextInt(s.length())));
        }
        StringBuffer result = new StringBuffer();
        result.append("Заказ")
                .append(" ")
                .append("№")
                .append(" ")
                .append(phoneNumber);
        return result.toString();
    }

    public static int totalSumForCart(Order order) {
        int totalPrice = 0;
        Set<OrderItem> orderItems = order.getOrderItems();
        totalPrice = orderItems
                .stream()
                .map(orderItem -> orderItem
                        .getService()
                        .getPrice()).mapToInt(Integer::intValue).sum();
        return totalPrice;
    }
}
