package com.mycompany.WashingApp.jpaExample;

import com.mycompany.WashingApp.entity.Review;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepo extends JpaRepository<Review, Integer>, ReviewDao {

}
