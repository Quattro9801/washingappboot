package com.mycompany.WashingApp.jpaExample;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Review;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

public interface ReviewDao {
    Review createReview(CarWash carWash, Review review);

}
