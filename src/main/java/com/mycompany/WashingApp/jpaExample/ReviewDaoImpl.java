package com.mycompany.WashingApp.jpaExample;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Review;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ReviewDaoImpl<T> implements ReviewDao {

    @Autowired
    EntityManager transactionManager;
    @Override
    public Review createReview(CarWash carWash, Review review) {
        review.setCarWash(carWash);
        transactionManager.merge(review);
        return review ;
    }



}
