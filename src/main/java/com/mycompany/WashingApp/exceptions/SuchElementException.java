package com.mycompany.WashingApp.exceptions;

public class SuchElementException extends RuntimeException {
    public SuchElementException(String message) {
        super(message);
    }
}
