package com.mycompany.WashingApp.exceptions.exceptionHandler;

import com.mycompany.WashingApp.exceptions.ErrorPojo;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import com.mycompany.WashingApp.exceptions.SuchElementException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

@ControllerAdvice//благодаря этой аннотации, обработка исключений может происходить в любом контроллере
public class GlobalHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorPojo> handleException(NoSuchElementException exception) { //exception - на который должен реагировать данный метод
        //IncorrectUserData - тип объекта,который добавляется в responseBody
        ErrorPojo incorrectUserData = new ErrorPojo();
        incorrectUserData.setTimestamp(LocalDateTime.now());
        incorrectUserData.setStatusCode(HttpStatus.NOT_FOUND.value());
        incorrectUserData.setStatusMessage(HttpStatus.NOT_FOUND);
        incorrectUserData.setMessage(exception.getMessage());
        return new ResponseEntity<>(incorrectUserData, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler
    public ResponseEntity<ErrorPojo> handleException(DateTimeException exception) { //exception - на который должен реагировать данный метод
        ErrorPojo errorPojo = new ErrorPojo();
        errorPojo.setMessage(exception.getMessage());
        errorPojo.setStatusCode(HttpStatus.BAD_REQUEST.value());
        errorPojo.setStatusMessage(HttpStatus.BAD_REQUEST);
        errorPojo.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(errorPojo, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorPojo> handleException(DateTimeParseException exception) { //exception - на который должен реагировать данный метод
        ErrorPojo errorPojo = new ErrorPojo();
        errorPojo.setMessage(exception.getMessage());
        errorPojo.setStatusCode(HttpStatus.BAD_REQUEST.value());
        errorPojo.setStatusMessage(HttpStatus.BAD_REQUEST);
        errorPojo.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(errorPojo, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorPojo> handleException(SuchElementException exception) { //exception - на который должен реагировать данный метод
        ErrorPojo errorPojo = new ErrorPojo();
        errorPojo.setMessage(exception.getMessage());
        errorPojo.setStatusCode(HttpStatus.CONFLICT.value());;
        errorPojo.setStatusMessage(HttpStatus.CONFLICT);
        errorPojo.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(errorPojo, HttpStatus.CONFLICT);
    }


  /*  @ExceptionHandler
    public ResponseEntity<IncorrectUserData> handleException(Exception exception) { //exception - на который должен реагировать данный метод
        //IncorrectUserData - тип объекта, котрый добавляется в responseBody
        IncorrectUserData incorrectUserData = new IncorrectUserData();
        incorrectUserData.setMessage(exception.getMessage());
        return new ResponseEntity<>(incorrectUserData, HttpStatus.BAD_REQUEST);
    }*/
}
