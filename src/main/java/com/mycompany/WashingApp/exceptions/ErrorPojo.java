package com.mycompany.WashingApp.exceptions;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class ErrorPojo {
    private LocalDateTime timestamp;
    private int statusCode;
    private HttpStatus statusMessage;
    private String message;

    public ErrorPojo() {
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(HttpStatus statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
