package com.mycompany.WashingApp.exceptions;

public class NoSuchElementException extends RuntimeException {
    public NoSuchElementException(String message) { //класс ответственный за выброс исключения
        super(message);
    }
}
