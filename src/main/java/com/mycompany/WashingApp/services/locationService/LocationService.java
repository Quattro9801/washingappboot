package com.mycompany.WashingApp.services.locationService;

import com.mycompany.WashingApp.entity.Location;
import org.springframework.stereotype.Component;

import java.util.List;


public interface LocationService {
    Location createLocation(Location location);
    List<Location> getAllLocations();
    Location getLocationById(Integer id);
    Location updateLocationById(Integer id, Location location);
}
