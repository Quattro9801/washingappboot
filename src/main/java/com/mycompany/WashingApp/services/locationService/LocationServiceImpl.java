package com.mycompany.WashingApp.services.locationService;

import com.mycompany.WashingApp.DAO.LocationDao;
import com.mycompany.WashingApp.entity.Location;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LocationServiceImpl implements LocationService {
    @Autowired
    LocationDao locationDao;
    @Override
    public Location createLocation(Location location) {
        return locationDao.createLocation(location);
    }

    @Override
    public List<Location> getAllLocations() {
        return locationDao.getAllLocations();
    }

    @Override
    public Location getLocationById(Integer id) {
        Location location = locationDao.getLocationById(id);
        if (location == null) {
            throw new NoSuchElementException("There is no location with id = " + id);
        }
        return location;
    }

    @Override
    public Location updateLocationById(Integer id, Location location) {
        Location currentLoc = getLocationById(id);
        return locationDao.updateLocationById(location, currentLoc);
    }
}
