package com.mycompany.WashingApp.services.userService;

import com.mycompany.WashingApp.DAO.UserDao;
import com.mycompany.WashingApp.entity.User;
import com.mycompany.WashingApp.entity.UserWithEmail;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImplService implements UserService {
    @Autowired
    private UserDao userDao;

    public User addUser(User user) {
        return userDao.addUser(user);
    }

    @Override
    public void removeUserById(Integer id) {
        userDao.removeUserById(id);
    }

    public User updateUserById(Integer id, User user) {
        return userDao.updateUserById(id, user);
    }

    @Override
    public User updateEmail(Integer id, UserWithEmail user) {
        return userDao.updateEmail(id, user);
    }

    @Override
    public User getUserTest(Integer id) {
        return userDao.getUserTest(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public User getUserById(Integer id) {
        User user = userDao.getUserById(id);
        if (user == null) {
            throw new NoSuchElementException("There is no user with id " + id + " in Database");
        }
        return user;
    }

    @Override
    public User getUserByIdTest(Integer id) {
        return null;
    }
}

/*    public User getUserByIdTest(int id) {
        User user = userDao.getById(id);
        if (user == null) {
            throw new NoSuchElementException("There is no user with id " +  id + " in Database");
        }
        return user;
    }
}*/
