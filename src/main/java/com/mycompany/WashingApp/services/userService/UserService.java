package com.mycompany.WashingApp.services.userService;

import com.mycompany.WashingApp.entity.User;
import com.mycompany.WashingApp.entity.UserWithEmail;

import java.util.List;

public interface UserService {
    public User addUser(User user);
    public User getUserById(Integer id );
    public User getUserByIdTest(Integer id );
    public List<User> getAllUsers();
    public void removeUserById(Integer id);
    public User updateUserById(Integer id, User user);
    User updateEmail(Integer id, UserWithEmail user);
    User getUserTest(Integer id);
}
