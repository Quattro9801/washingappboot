package com.mycompany.WashingApp.services.Service_For_Wash;

import java.util.List;

public interface Service {
   com.mycompany.WashingApp.entity.Service addService(com.mycompany.WashingApp.entity.Service service);
   List<com.mycompany.WashingApp.entity.Service> getAllServices();
   com.mycompany.WashingApp.entity.Service getServiceById(Integer id);
   com.mycompany.WashingApp.entity.Service updateServiceById(Integer id, com.mycompany.WashingApp.entity.Service service);
   void deleteServiceById(Integer id);
}
