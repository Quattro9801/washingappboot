package com.mycompany.WashingApp.services.modelCarService;

import com.mycompany.WashingApp.entity.ModelCar;

public interface ModelCarService {
    ModelCar getModelCarById(int id);
}
