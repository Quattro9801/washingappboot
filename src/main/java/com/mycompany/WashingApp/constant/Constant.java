package com.mycompany.WashingApp.constant;

import com.mycompany.WashingApp.entity.Order;

import java.time.format.DateTimeFormatter;

public class Constant {
    public static final String HIBERNATE_DIALECT = "hibernate.dialect";
    public static final String HIBERNATE_DIALECT_POSTGRESQL = "org.hibernate.dialect.PostgreSQLDialect";
    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

}
