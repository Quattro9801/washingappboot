package com.mycompany.WashingApp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@NoArgsConstructor
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Integer orderId;
    @Column(name = "name")
    private String name;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE},  fetch = FetchType.EAGER)
    @JoinColumn(name = "order_status_id")
    private StatusOrder statusOrder;

    @Column(name = "created_date")
    private LocalDateTime createdDate;
    @Column(name = "date_of_receipt")
    private LocalDateTime dateOfReceipt;
    @Column(name = "total_price")
    private Integer totalPrice;
    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "user_id")//аннотация говорит где искать связь между таблицами
    private User user;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE},  fetch = FetchType.EAGER)
    @JoinColumn(name = "car_wash_id")
    private CarWash carWash;

    @OneToMany(mappedBy = "order",
            fetch = FetchType.EAGER,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH},
            orphanRemoval = true)//orphanRemoval = true указывает, что все объекты OI, которые не имеют ссылки на ORDER должны быть удалены,
    private Set<OrderItem> orderItems;



    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StatusOrder getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(StatusOrder statusOrder) {
        this.statusOrder = statusOrder;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public LocalDateTime getDateOfReceipt() {
        return dateOfReceipt;
    }

    public void setDateOfReceipt(LocalDateTime dateOfReceipt) {
        this.dateOfReceipt = dateOfReceipt;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Car getCar() {
        return car;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public CarWash getCarWash() {
        return carWash;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setCarWash(CarWash carWash) {
        this.carWash = carWash;
    }

    public User getUser() {
        return user;
    }

    public Set<OrderItem> addOrderItemToSO(OrderItem orderItem) {
        if (orderItems == null) {
            orderItems = new HashSet<>();
            orderItems.add(orderItem);
        }
        orderItems.add(orderItem);
        return orderItems;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", statusOrder=" + statusOrder +
                ", createdDate=" + createdDate +
                ", dateOfReceipt=" + dateOfReceipt +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
