package com.mycompany.WashingApp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Set;

@Entity
@NoArgsConstructor
@Table(name = "status_orders")
public class StatusOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "status_order_id")
    private Integer id;
    @Column(name = "status")
    private String status;
    @OneToMany(mappedBy = "statusOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Order> orders;

    public StatusOrder(int id, String status) {
        this.id = id;
        this.status = status;
    }

    public StatusOrder(String status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "StatusOrder{" +
            "id=" + id +
            ", status='" + status + '\'' +
            '}';
    }
}
