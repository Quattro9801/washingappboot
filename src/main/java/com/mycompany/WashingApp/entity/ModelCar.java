package com.mycompany.WashingApp.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = "models_cars")
public class ModelCar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "model_id")
    private Integer modelId;
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "modelCar", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Car> cars;

    public ModelCar(String name, List<Car> cars) {
        this.name = name;
        this.cars = cars;
    }

    public Integer getModelId() {
        return modelId;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ModelCar{" +
            "modelId=" + modelId +
            ", name='" + name + '\'' +
            '}';
    }
}
