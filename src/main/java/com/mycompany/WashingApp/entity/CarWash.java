package com.mycompany.WashingApp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
@NoArgsConstructor
@Table(name = "car_washes")
public class CarWash {
    @Id
    @Column(name = "car_wash_id")
    private Integer carWashId;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "car_wash_name")
    private String carWashName;
    @Column(name = "description")
    private String description;
    @Column(name = "working_hours")
    private String workingHours;

    @OneToOne(mappedBy = "carWash", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private Location location;

    @OneToMany(mappedBy = "carWash", cascade = {CascadeType.MERGE, CascadeType.PERSIST},  fetch = FetchType.LAZY)
    //@Fetch(value = FetchMode.SELECT)
    private List<Order> orders;

    @OneToMany(mappedBy = "carWash", cascade = CascadeType.ALL)
    private List<Review> reviews;
    public CarWash(String phoneNumber, String carWashName, String description, String workingHours) {
        this.phoneNumber = phoneNumber;
        this.carWashName = carWashName;
        this.description = description;
        this.workingHours = workingHours;
    }

    public Integer getCarWashId() {
        return carWashId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCarWashName() {
        return carWashName;
    }

    public void setCarWashName(String carWashName) {
        this.carWashName = carWashName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public Location getLocation() {
        return location;
    }

    public void setCarWashId(Integer carWashId) {
        this.carWashId = carWashId;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    /* @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarWash carWash = (CarWash) o;
        if (getLocation() == null || carWash.getLocation() == null) {
            return Objects.equals(location, carWash.getLocation());
        }
        return getLocation().getAddressName().equals(carWash.getLocation().getAddressName());
    }

    @Override
    public int hashCode() {
        int result = 17;
        if (getLocation() != null) {
            if (getLocation().getAddressName() !=null) {
                result = 31 * result + getLocation().getAddressName().hashCode();
            }
        }
        return result;
    }*/

    @Override
    public String toString() {
        return "CarWash{" +
                "carWashId=" + carWashId +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", carWashName='" + carWashName + '\'' +
                ", description='" + description + '\'' +
                ", workingHours='" + workingHours + '\'' +
                '}';
    }
}
