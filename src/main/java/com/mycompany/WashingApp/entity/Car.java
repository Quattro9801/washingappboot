package com.mycompany.WashingApp.entity;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;


import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = "cars" )
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id")
    private Integer carId;
    @Column(name = "car_name")
    private String carName;
    @Column(name = "model")
    private String model;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")//аннотация говорит где искать связь между таблицами
    private User user;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Order> orders;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "type_car_id")//аннотация говорит где искать связь между таблицами
    private TypeCar typeCar;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "model_id")//аннотация говорит где искать связь между таблицами
    private ModelCar modelCar;

    public Car(String carName, String model, User user, TypeCar typeCar, ModelCar modelCar) {
        this.carName = carName;
        this.model = model;
        this.user = user;
        this.typeCar = typeCar;
        this.modelCar = modelCar;
    }

    public Car(String carName, String model, TypeCar typeCar, ModelCar modelCar) {
        this.carName = carName;
        this.model = model;
        this.typeCar = typeCar;
        this.modelCar = modelCar;
    }

    public Integer getCarId() {
        return carId;
    }

    public String getCarName() {
        return carName;
    }

    public String getModel() {
        return model;
    }

    public ModelCar getModelCar() {
        return modelCar;
    }

    public TypeCar getTypeCar() {
        return typeCar;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void setTypeCar(TypeCar typeCar) {
        this.typeCar = typeCar;
    }

    public void setModelCar(ModelCar modelCar) {
        this.modelCar = modelCar;
    }

    @Override
    public String toString() {
        return "Car{" +
            "carId=" + carId +
            ", carName='" + carName + '\'' +
            ", model='" + model + '\'' +
            '}';
    }
}
