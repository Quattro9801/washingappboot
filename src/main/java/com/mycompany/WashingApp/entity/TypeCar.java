package com.mycompany.WashingApp.entity;

import lombok.NoArgsConstructor;

import jakarta.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = "type_cars")
public class TypeCar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private Integer typeId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "typeCar", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Car> cars;

    public TypeCar(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "TypeCar{" +
            "typeId=" + typeId +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            '}';
    }
}
