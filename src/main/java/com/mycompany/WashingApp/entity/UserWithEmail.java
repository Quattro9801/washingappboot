package com.mycompany.WashingApp.entity;

public class UserWithEmail {
    private String email;
    public UserWithEmail() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserWithEmail{" +
            "email='" + email + '\'' +
            '}';
    }
}
