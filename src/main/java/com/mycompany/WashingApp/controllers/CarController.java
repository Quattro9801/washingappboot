package com.mycompany.WashingApp.controllers;

import com.mycompany.WashingApp.entity.Car;
import com.mycompany.WashingApp.entity.User;
import com.mycompany.WashingApp.services.Service;
import com.mycompany.WashingApp.services.carService.CarService;
import com.mycompany.WashingApp.services.userService.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Tag(name = "Api for cars", description=" All methods for working with cars")
@RestController
@RequestMapping("/v1/carManagement")
public class CarController {
    @Autowired
    Service service;
    @PostMapping("users/{id}")
    public ResponseEntity<Car> createCar(@PathVariable Integer id,
                                        @RequestParam("modelId") Integer modelId,
                                        @RequestParam("typeCarId") Integer typeCarId,
                                         @RequestBody Car car) {
        UserService userService = service.getUserService();//нашли сервис
        User user = userService.getUserById(id); //нашли юзера
        return new ResponseEntity<>(service.getCarService().createCar(car,user,
                service.getModelCarService().getModelCarById(modelId), service.getTypeCarService().getTypeCarById(typeCarId)), HttpStatus.CREATED);
    }
    @GetMapping("/cars/{id}")
    public Car getCarById(@PathVariable("id") Integer id) {
        CarService carService = service.getCarService();
        return carService.getCarById(id);
    }

    @GetMapping("cars/user/{id}")
    List<Car> getAllCarsByUser (@PathVariable("id") Integer id) {
        service.getUserService().getUserById(id);
        return service.getCarService().getAllCarsByUser(id);
    }
}
