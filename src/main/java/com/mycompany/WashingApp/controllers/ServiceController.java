package com.mycompany.WashingApp.controllers;

import com.mycompany.WashingApp.services.Service;
import jakarta.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/serviceManagement")
public class ServiceController {

    @Autowired
    Service service;

    @GetMapping("/services")
    public List<com.mycompany.WashingApp.entity.Service> getAllServices() {
        return service.getService().getAllServices();
    }

    @PostMapping("/services")
    public ResponseEntity<com.mycompany.WashingApp.entity.Service> createService(@RequestBody com.mycompany.WashingApp.entity.Service serviceEnt) {
        return new ResponseEntity<>(service.getService().addService(serviceEnt), HttpStatus.CREATED);
    }

    @GetMapping("/services/{id}")
    public com.mycompany.WashingApp.entity.Service getServiceById(@PathVariable Integer id) {
        return service.getService().getServiceById(id);
    }

    @PutMapping("/services/{id}")
    public com.mycompany.WashingApp.entity.Service updateServiceById(@PathVariable("id") Integer id, @RequestBody com.mycompany.WashingApp.entity.Service serviceEnt) {
        return service.getService().updateServiceById(id, serviceEnt);
    }

    @DeleteMapping("/services/{id}")
    public void deleteServiceById(@PathVariable("id") Integer id) {
            // под ? нужен ли этот метод
    }

}
