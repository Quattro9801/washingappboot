package com.mycompany.WashingApp.controllers;

import com.mycompany.WashingApp.entity.Location;
import com.mycompany.WashingApp.services.Service;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
@Tag(name = "Api for locations", description=" All methods for working with locations")
@RestController
@RequestMapping("/v1/locationManagement")
public class LocationController {
    @Autowired
    Service service;

    @PostMapping("/locations")
    public ResponseEntity<Location> createLocation(@RequestBody Location location) {
        return new ResponseEntity<>(service.getLocationService().createLocation(location), HttpStatus.CREATED);
    }

    @GetMapping("/locations")
    public List<Location> getAllLocations() {
        return service.getLocationService().getAllLocations();
    }

    @GetMapping("/locations/{id}")
    public Location getLocationById(@PathVariable Integer id) {
        return service.getLocationService().getLocationById(id);
    }

    @PutMapping("/locations/{id}")
    public Location updateLocationById(@PathVariable("id") Integer id,
                                       @RequestBody Location location) {
        return service.getLocationService().updateLocationById(id, location);
    }
}
