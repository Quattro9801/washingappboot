package com.mycompany.WashingApp.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.mycompany.WashingApp.constant.Constant;
import com.mycompany.WashingApp.entity.*;
import com.mycompany.WashingApp.model.ServiceModel;
import com.mycompany.WashingApp.model.StatusOrderModel;
import com.mycompany.WashingApp.services.carService.CarService;
import com.mycompany.WashingApp.services.carService.CarServiceImpl;
import com.mycompany.WashingApp.services.Service;
import com.mycompany.WashingApp.services.carWashService.CarWashService;
import com.mycompany.WashingApp.services.orderService.OrderService;
import com.mycompany.WashingApp.services.userService.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.List;
@Tag(name = "Api for orders", description=" All methods for working with orders")
@RestController
@RequestMapping("v1/orderManagement")
public class OrderController {
    private final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(OrderController.class);
    @Autowired
    Service service;
    @PostMapping("/users/{id}") // dateFormat 2024-07-10 14:59:00
    public ResponseEntity<Order> createOrder(@PathVariable("id") Integer id,
                                             @RequestParam("carId") Integer carId,
                                             @RequestParam("carWashId") Integer carWashId,
                                             @RequestParam("dateOfReceipt") String dateOfReceipt) {
        LocalDateTime dateTime = null;
        try {
            dateTime = LocalDateTime.parse(dateOfReceipt, Constant.formatter);
        } catch (DateTimeException e) {
            LOGGER.info("Incorrect format of date.Insert correct format of date");
            throw new DateTimeException("Invalid date format" + " " + dateOfReceipt + ". "+ "Insert correct date value!");
        }
        UserService userService = service.getUserService();//нашли сервис
        User user = userService.getUserById(id); //нашли юзера

        CarService carService = service.getCarService();
        Car car = carService.getCarById(carId);

        CarWashService carWashService = service.getCarWashService();
        CarWash carWash = carWashService.getCarWashById(carWashId);

        OrderService orderService = service.getOrderService();

        return new ResponseEntity<>(orderService.createOrder(user, carWash, car, dateTime), HttpStatus.CREATED);
    }

    @GetMapping("/salesOrders/{id}")
    public Order getOrderById(@PathVariable("id") Integer id){
        OrderService orderService = service.getOrderService();
        return orderService.getOrderById(id);
    }

    @GetMapping("/salesOrders/user/{userId}")
    public List<Order> getOrdersByUserId(@PathVariable("userId") Integer id){
        UserService userService = service.getUserService();
        userService.getUserById(id);
        OrderService orderService = service.getOrderService();
        return orderService.getOrdersByUserId(id);
    }

    @PostMapping("/salesOrder/{id}/orderItem")
    public Order addOrderItemToSO(@PathVariable("id") Integer id,
                                                 @RequestBody ServiceModel serviceModel){
        OrderService orderService = service.getOrderService();
        Order order = orderService.getOrderById(id);
        return orderService.addOrderItemToSo(order, serviceModel);//создать класс SERVICE с ID
    }

    @GetMapping ("/salesOrder/orderItem/{id}")
    public OrderItem getOrderItem(@PathVariable("id") Integer id) {
        OrderService orderService = service.getOrderService();
        return orderService.getOrderItemById(id);
    }

    @DeleteMapping("/salesOrder/{id}/orderItem")
    public void deleteOrderItem(@PathVariable("id") Integer id,
                                @RequestBody OrderItem orderItem) {
        OrderService orderService = service.getOrderService();;
        orderService.deleteOrderItem(orderItem, id);
    }

    @PatchMapping("/salesOrder/{id}/status")
    public Order changeOrderStatus(@PathVariable("id") Integer id,
                                   @RequestBody StatusOrderModel statusOrder) {
        OrderService orderService = service.getOrderService();
        StatusOrder statusOrderNew = orderService.getStatusOrderById(statusOrder.getStatusOrderId());
        return orderService.changeOrderStatus(id, statusOrderNew);
    }

    @PostMapping("/orderStatuses")
    public ResponseEntity<StatusOrder> createStatus(@RequestBody StatusOrder statusOrder) {
        return new ResponseEntity<>(service.getOrderService().createStatusOrder(statusOrder), HttpStatus.CREATED);
    }

    @GetMapping("/orderStatuses")
    public List<StatusOrder> getAllStatuses() {
        return service.getOrderService().getAllStatuses();
    }

    @GetMapping("/orderStatuses/{id}")
    public StatusOrder getOrderStatusById(@PathVariable("id") Integer id) {
        return service.getOrderService().getStatusOrderById(id);
    }

    //@Scheduled(fixedRate = 1000)
    public void scheduleFixedRateTask() {
        System.out.println(
                "Fixed rate task - " + System.currentTimeMillis() / 1000);
    }


}
