package com.mycompany.WashingApp.controllers;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Review;
import com.mycompany.WashingApp.jpaExample.ReviewRepo;
import com.mycompany.WashingApp.services.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@Slf4j
@RestController
@RequestMapping("/v1/reviewManagement")
public class ReviewController {
    @Autowired
    Service service;

    @PostMapping("/reviews")
    public ResponseEntity<Review> createReview(@RequestParam("carWashId") Integer carWashId,
                                              @RequestBody Review review) {

        CarWash carWash = service.getCarWashService().getCarWashById(carWashId);
        return new ResponseEntity<>(service.getReviewService().createReview(carWash, review), HttpStatus.CREATED);

    }
    @GetMapping("/reviews")
    public List<Review> getAllReviews() {
        List<Review> reviews = service.getReviewService().getAllReviews();
        return reviews;
    }
    @GetMapping("/reviews/{id}")
    public Optional<Review> getReviewsById(@PathVariable("id") Integer id) {
        return service.getReviewService().getReviewById(id);
    }

    @DeleteMapping("/reviews/{id}")
    public void deleteReview(@PathVariable("id") Integer id) {
        Optional<Review> review = service.getReviewService().getReviewById(id);
        service.getReviewService().deleteReview(id);
        log.info("Удаление успешно {}", review);
    }
}
