package com.mycompany.WashingApp.controllers;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Location;
import com.mycompany.WashingApp.services.Service;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@Tag(name = "Api for car washes", description=" All methods for working with car washes")
@RestController
@RequestMapping("/v1/carWashManagement")
public class CarWashController {

    @Autowired
    Service service;

    @PostMapping ("/carWashes/location/{id}")
    public CarWash createCarWash(@RequestBody CarWash carWash, @PathVariable("id") Integer id) {
        Location location = service.getLocationService().getLocationById(id);//	ALTER TABLE car_washes ADD CONSTRAINT fk_location FOREIGN KEY (car_wash_id_id) REFERENCES location(location_id
       return service.getCarWashService().createCarWash(carWash, location);
    }

    @GetMapping("/carWashes/{id}")
    CarWash getCarWashById(@PathVariable("id") Integer id) {
        return service.getCarWashService().getCarWashById(id);
    }

    @GetMapping ("/carWashes")
    public List<CarWash> getAllCarWashes()
    {
        return service.getCarWashService().getAllCarWashes();
    }
    @PutMapping("/carWashes/{id}")
    public CarWash updateCarWash(@PathVariable("id") Integer id, @RequestBody CarWash carWash) {
        return service.getCarWashService().updateCarWashById(id, carWash);
    }
}
