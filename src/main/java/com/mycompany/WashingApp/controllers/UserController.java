package com.mycompany.WashingApp.controllers;

import com.mycompany.WashingApp.DAO.UserDao;
import com.mycompany.WashingApp.constant.Constant;
import com.mycompany.WashingApp.entity.User;
import com.mycompany.WashingApp.entity.UserWithEmail;
import com.mycompany.WashingApp.services.Service;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
//http://localhost:8082/api/swagger-ui/index.html
@Slf4j
@Tag(name = "Api for users", description=" all methods for working with users")
@RestController
@RequestMapping("/v1/customerManagement")
public class UserController {

    @Autowired
    Service service;
    @Autowired
    UserDao userDao;

    @Operation(
        summary = "Get all users list",
        description = "Let to show all list of users"
    )
    @GetMapping("/users")
    public List<User> getUsers(){
       log.info("GetAllusers execution");
        return service.getUserService().getAllUsers();
    }
    @Operation(
        summary = "Get user by id",
        description = "Let to show only one user"
    )
    @GetMapping("/users/{id}")
    public User getUserById(@PathVariable("id") Integer id) {
        return service.getUserService().getUserById(id);
    }

    public User getUserByIdTest1(@PathVariable("id") Integer id) {
        return service.getUserService().getUserById(id);
    }

    @GetMapping("/usersTest/{id}")
    public User getUserByIdTest(@PathVariable("id") Integer id) {
        return service.getUserService().getUserTest(id);
    }

    @DeleteMapping("/users/{id}")
    public void removeUserById(@PathVariable("id") Integer id){
        service.getUserService().removeUserById(id);
    }

    @PostMapping("/users") // dateFormat 1998-02-21
    public ResponseEntity<User> addUser(@RequestBody User user) {
        User newUser = service.getUserService().addUser(user);
        log.info("aad user");
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }
    @PutMapping("/users/{id}")
    public User updateUserById(
                               @RequestBody User user,
                               @PathVariable("id") Integer id) {
        User updatedUser = null;
        try {
            updatedUser = service.getUserService().updateUserById(id,user);
        } catch (HttpMessageNotReadableException e) {
           log.info("Incorrect format of date.Insert correct format of date");
        };
        return updatedUser;
    }

    @PatchMapping("/users/{id}")
    public User updateEmailById(
            @RequestBody UserWithEmail user,
            @PathVariable("id") Integer id) {
        User updatedUser = service.getUserService().updateEmail(id, user );
        return updatedUser;
    }

    @GetMapping("/getAllUsers")
    public List<User> getAllUserstest() {
       return userDao.getAllUsersTest();
    }


}
