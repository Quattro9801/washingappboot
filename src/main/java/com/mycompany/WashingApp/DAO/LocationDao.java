package com.mycompany.WashingApp.DAO;

import com.mycompany.WashingApp.entity.Location;

import java.util.List;

public interface LocationDao {
    Location createLocation(Location location);
    List<Location> getAllLocations();
    Location getLocationById(Integer id);
    Location updateLocationById(Location location, Location current);
}
