package com.mycompany.WashingApp.DAO;

import com.mycompany.WashingApp.entity.TypeCar;

public interface TypeCarDao {
    TypeCar getTypeCarById(Integer id);
}
