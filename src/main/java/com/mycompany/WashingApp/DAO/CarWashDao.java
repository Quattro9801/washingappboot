package com.mycompany.WashingApp.DAO;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Location;

import java.util.List;

public interface CarWashDao {
    CarWash createCarWash(CarWash carWash, Location location);
    CarWash getCarWashById(Integer id);
    List<CarWash> getAllCarWashes();
    CarWash updateCarWash(Integer id, CarWash carWash);
    void deleteCarWash(Integer id);
}
