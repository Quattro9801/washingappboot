package com.mycompany.WashingApp.DAO;

import com.mycompany.WashingApp.entity.*;
import com.mycompany.WashingApp.model.ServiceModel;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface OrderDao {
    Order createOrder(User user, CarWash carWash, Car car, LocalDateTime date);
    Order getOrderById (Integer id);
    List<Order> getOrdersByUserId(Integer userId);
    Order addOrderItemToSalesOrder(Order order, ServiceModel service);
    List<Order> getAllOrders();
    void deleteOrderItem(OrderItem orderItem, Integer orderId);
    OrderItem getOrderItemById(Integer id);
    Order changeOrderStatus(Order order, StatusOrder status);
    StatusOrder getStatusOrderById(Integer id);
    List<StatusOrder> getAllStatuses();
    StatusOrder getOrderStatusById(Integer id);
    StatusOrder createStatusOrder(StatusOrder statusOrder);



}
