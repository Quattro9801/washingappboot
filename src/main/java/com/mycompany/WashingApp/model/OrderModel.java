package com.mycompany.WashingApp.model;

import com.mycompany.WashingApp.entity.OrderItem;

import java.util.Date;
import java.util.List;

public class OrderModel {
    private int orderId;
    private String status;
    private Date createdDate;
    private int totalPrice;
    List<OrderItem> orderItemList;

    public OrderModel(int orderId, String status, Date createdDate, int totalPrice, List<OrderItem> orderItemList) {
        this.orderId = orderId;
        this.status = status;
        this.createdDate = createdDate;
        this.totalPrice = totalPrice;
        this.orderItemList=orderItemList;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getStatus() {
        return status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    @Override
    public String toString() {
        return "OrderModel{" +
                "orderId=" + orderId +
                ", status='" + status + '\'' +
                ", createdDate=" + createdDate +
                ", totalPrice=" + totalPrice +
                ", orderItemList=" + orderItemList +
                '}';
    }
}
