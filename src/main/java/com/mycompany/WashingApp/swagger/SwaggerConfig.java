package com.mycompany.WashingApp.swagger;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition (info = @Info(
    title = "Washing app",
    description = "Приложение Автомойка флекс", version = "1.0.0",
    contact = @Contact(
        name = "Netreba Ivan",
        email = "ivan_netr@mail.ru",
        url = "https://gitlab.com/Quattro9801"
    )
)
)
public class SwaggerConfig {
}
