package com.mycompany.WashingApp.camel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/camel")
public class Executor {
    @Autowired
    CamelMain camelMain;
    @Autowired
    ProdandConsumer prodandConsumer;
    @GetMapping("execute")
    public void execute() throws Exception {
        camelMain.run();
    } @GetMapping("executeProdAndCons")
    public void executeProdAndConsume() throws Exception {
        prodandConsumer.run();
    }
}
