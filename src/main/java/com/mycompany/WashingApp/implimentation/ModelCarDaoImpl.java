package com.mycompany.WashingApp.implimentation;

import com.mycompany.WashingApp.DAO.ModelCarDao;
import com.mycompany.WashingApp.entity.ModelCar;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
@Transactional
public class ModelCarDaoImpl implements ModelCarDao {
    @Autowired
    EntityManager transactionManager;
    @Override
    public void addModelCar(ModelCar modelCar) {

    }

    @Override
    public void deleteModelCar(Integer id) {

    }

    @Override
    public List<ModelCar> getAllModelCars() {
        return null;
    }

    @Override
    public ModelCar getModelCarById(Integer id) {
        ModelCar modelCar = transactionManager.find(ModelCar.class, id);
        return modelCar;
    }

    @Override
    public ModelCar updateModelCar(Integer id, ModelCar updatedModelCar) {
        return null;
    }

   /* @Autowired
    FactoryBuilder factoryBuilder;

    @Override
    public void addModelCar(ModelCar modelCar) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(modelCar);
            System.out.println("Added modelCar" + modelCar);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }

    }

    @Override
    public void deleteModelCar(int id) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            ModelCar modelCar = (ModelCar) session.get(ModelCar.class, id);
            session.delete(modelCar);
            System.out.println("Deleted User " + modelCar);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
    }

    @Override
    public List<ModelCar> getAllModelCars() {

        List<ModelCar> modelCars = null;

        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            modelCars = session.createQuery("FROM ModelCar").list();
        } finally {
            factoryBuilder.getSessionFactory().close();
        }
        return modelCars;
    }

    @Override
    public ModelCar getModelCarById(int id) {
        ModelCar modelCar = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            modelCar= (ModelCar) session.get(ModelCar.class, id);
            System.out.println("FindUserByID " + modelCar);
            session.getTransaction().commit();

        } finally {
            factoryBuilder.getSessionFactory().close();
        }
        return modelCar;
    }

    @Override
    public ModelCar updateModelCar(int id, ModelCar updatedModelCar) {
        ModelCar modelCar = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            modelCar= (ModelCar) session.get(ModelCar.class, id);
            modelCar.setName(updatedModelCar.getName());
            System.out.println("FindUserByID " + modelCar);
            session.getTransaction().commit();

        } finally {
            factoryBuilder.getSessionFactory().close();
        }

        return modelCar;
    }*/
}
