package com.mycompany.WashingApp.implimentation;

import jakarta.persistence.EntityManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLDataException;

@Slf4j
@Transactional
@Repository
public class CheckDbDAO {
    @Autowired
    EntityManager entityManager;
    //@Scheduled(fixedRate = 10000)
    public void checkConnection() throws SQLDataException {
        log.info("Checking DB state");
        try {

            entityManager.createNativeQuery("SELECT 1").getResultList().stream().findAny().orElse(null);
        }
        catch (Exception ex){
            log.warn("Problem with connection!!!");
            throw new SQLDataException();
        }
    }
}
