package com.mycompany.WashingApp.implimentation;

import com.mycompany.WashingApp.DAO.CarWashDao;
import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Location;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import com.mycompany.WashingApp.exceptions.SuchElementException;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class CarWashDaoImpl implements CarWashDao {
    @Autowired
    EntityManager transactionManager;
    @Override
    public CarWash createCarWash(CarWash carWash, Location location) {
        for (CarWash cW: getAllCarWashes()) {
            if (cW.getCarWashId() == location.getLocationId()) {
                throw new SuchElementException("Location with id =" + " " + location.getLocationId() + " already exists in Database");
            }
        }
        carWash.setCarWashId(location.getLocationId());
        carWash.setLocation(location);
        transactionManager.merge(carWash);
        return carWash;
    }

    @Override
    public CarWash getCarWashById(Integer id) {
        CarWash carWash = transactionManager.find(CarWash.class, id);
        if (carWash == null) {
            throw new NoSuchElementException("There is no carWash with id " + id + " in Database");
        }
        return carWash;
    }

    @Override
    public List<CarWash> getAllCarWashes() {
        List<CarWash> carWashList = transactionManager.createQuery("FROM CarWash", CarWash.class).getResultList();
        return carWashList;
    }


    @Override
    public CarWash updateCarWash(Integer id, CarWash carWash) {
        CarWash currentCarWash = getCarWashById(id);
        currentCarWash.setCarWashName(carWash.getCarWashName());
        currentCarWash.setPhoneNumber(carWash.getPhoneNumber());
        currentCarWash.setDescription(carWash.getDescription());
        currentCarWash.setWorkingHours(carWash.getWorkingHours());
        //currentCarWash.setLocation(carWash.getLocation());
        transactionManager.merge(currentCarWash);
        return currentCarWash;
    }

    @Override
    public void deleteCarWash(Integer id) {

    }
}
