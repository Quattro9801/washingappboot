package com.mycompany.WashingApp.implimentation;

import com.mycompany.WashingApp.DAO.OrderDao;
import com.mycompany.WashingApp.constant.Constant;
import com.mycompany.WashingApp.controllers.OrderController;
import com.mycompany.WashingApp.entity.*;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import com.mycompany.WashingApp.logic.ExternalOrderOperation;
import com.mycompany.WashingApp.model.ServiceModel;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Repository
@Transactional
public class OrderDaoImpl implements OrderDao {
    private final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(OrderDaoImpl.class);
    @Autowired
    EntityManager transactionManager;
    @Override
    public Order createOrder(User user, CarWash carWash, Car car, LocalDateTime dateOfReceipt) {
        Order order = new Order();
        order.setUser(user);
        order.setCarWash(carWash);
        order.setCar(car);
        order.setName(ExternalOrderOperation.getRandomOrderName());
        StatusOrder statusOrder = transactionManager.find(StatusOrder.class, 1);
        order.setStatusOrder(statusOrder);


        LocalDateTime date = LocalDateTime.now();
        String res = Constant.formatter.format(date);
        LocalDateTime newLoc = LocalDateTime.parse(res, Constant.formatter);

        order.setCreatedDate(newLoc);
        order.setDateOfReceipt(dateOfReceipt);

        transactionManager.merge(order);
        return order;
    }

    @Override
    public Order getOrderById(Integer id) {

        Order order = transactionManager.find(Order.class, id);
        System.out.println("FindOrder " + order);
        return order;
    }

   /* @Override
    public List<OrderModel> getOrdersByUserId(int id) {
        List<OrderModel> orderList = null;
        List<String> list = new ArrayList<>();
       *//* Query query = getSession().createQuery("Select ioObject from IoObject as ioObject where ioObject.subTotalVariable.varId in (:varList)")
                .setParameterList("varList",variableList);*//*

        try {
            Session session = transactionManager.getSessionFactory().getCurrentSession();
           *//* Query<String> userQuery = session.createQuery("SELECT o.status from Order o where o.orderId=:orderId").setParameter("orderId", id);*//*
           // Query<OrderModel> userQuery = session.createQuery("FROM Order o, User u WHERE u.id = o.user.id and o.user.id=:id").setParameter("id",id);
            Query<OrderModel> userQuery = session.createQuery("SELECT new com.mycompany.hibernate.washing.model.OrderModel (o.orderId as id,o.status, o.createdDate, o.totalPrice) FROM Order o, User u WHERE u.id = o.user.id and o.user.id=:id").setParameter("id",id);
            //https://stackoverflow.com/questions/23122846/query-returning-object-instead-of-entity
            String status = userQuery.getResultList().toString();

           // list = userQuery.getResultList();
            orderList = userQuery.getResultList();
            System.out.println(orderList);

        } catch(HibernateException exception){
            System.out.println("Problem creating session factory");
            exception.printStackTrace();
        }
        return orderList;
    }*/
    @Override
    public List<Order> getOrdersByUserId(Integer userId){
        List<Order> list = null;
        Query userQuery = transactionManager.createQuery(" FROM Order o where o.user.id  = :id", Order.class).setParameter("id",userId);
        list = userQuery.getResultList();
        System.out.println();
        return list;
    }

    @Override
    public Order addOrderItemToSalesOrder(Order order, ServiceModel service) {
        //https://stackoverflow.com/questions/2302802/how-to-fix-the-hibernate-object-references-an-unsaved-transient-instance-save
        OrderItem orderItem = new OrderItem();
        orderItem.setOrder(order);
        Service service1 = transactionManager.find(Service.class, service.getServiceId());
        if (service1 == null) {
            throw new NoSuchElementException("Service with id " + service.getServiceId() + " " + "not found");
        }
        orderItem.setService(service1);
        //transactionManager.merge(orderItem);
        order.addOrderItemToSO(orderItem);
        transactionManager.merge(order);
        System.out.println(order);
        System.out.println("Oi was created)");
        System.out.println("---------------------------------");
        order.setTotalPrice(ExternalOrderOperation.totalSumForCart(order));
        transactionManager.merge(order);

        return order;
    }

/*    public int totalSumForCart(Order order) {
        int totalPrice = 0;
        List<OrderItem> orderItems = order.getOrderItems();
        totalPrice = orderItems
            .stream()
            .map(orderItem -> orderItem
                .getService()
                .getPrice()).mapToInt(Integer::intValue).sum();
        return totalPrice;
    }*/

    @Override
    public List<Order> getAllOrders() {
        return null;
    }

    @Override
    public void deleteOrderItem(OrderItem orderItem, Integer orderId) {

        transactionManager.createQuery(" DELETE FROM OrderItem o where o.orderItemId  = :id").setParameter("id", orderItem.getOrderItemId()).executeUpdate();
        Order order = getOrderById(orderId);
        order.setTotalPrice(ExternalOrderOperation.totalSumForCart(order));
        transactionManager.merge(order);


        // 2 способ
    /*    Order order = transactionManager.find(Order.class, orderId);
        order.getOrderItems().removeIf((OrderItem orderItem1) -> {
            if (orderItem1.getOrderItemId() == orderItem.getOrderItemId()){
                orderItem1.setOrder(null);
                transactionManager.remove(orderItem1);
                return true;
            }
            return false;
        } );*/

    }

    @Override
    public OrderItem getOrderItemById(Integer id) {
        OrderItem orderItem =  transactionManager.find(OrderItem.class, id);
        return orderItem;
    }

    @Override
    public Order changeOrderStatus(Order order, StatusOrder status) {
       order.setStatusOrder(status);
       transactionManager.merge(order);
       return order ;
    }

    @Override
    public StatusOrder getStatusOrderById(Integer id) {
        StatusOrder statusOrder = transactionManager.find(StatusOrder.class, id);
        return statusOrder;
    }

    @Override
    public List<StatusOrder> getAllStatuses() {
        Query query = transactionManager.createQuery("FROM StatusOrder", StatusOrder.class);
        List<StatusOrder> list = query.getResultList();
        return list;
    }

    @Override
    public StatusOrder getOrderStatusById(Integer id) {
        StatusOrder statusOrder = transactionManager.find(StatusOrder.class, id);
        return statusOrder ;
    }

    @Override
    public StatusOrder createStatusOrder(StatusOrder statusOrder) {
        transactionManager.merge(statusOrder);
        return statusOrder;
    }


}
